package com.testng.test.base;

import org.testng.ITestContext;
import org.testng.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class BaseTest {
	
	@BeforeSuite(alwaysRun=true)
	public void beforeSuite(ITestContext context)
	{
		//String value = context.getCurrentXmlTest().getParameter("Car");
		String value = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("Car"); 
	    System.err.println("webdriver.car.name = " + value);
	    System.out.println(System.getProperty("browser"));
	    if(System.getProperty("browser")==null)
	    {
	    	System.out.println("Browser is :"+value);
	    }else
	    {
	    	System.out.println("Browser is :"+System.getProperty("browser"));
	    }
	    
		System.out.println("Executing before Suite ...");
	}
	
	@AfterSuite(alwaysRun=true)
	public void afterSuite()
	{
		System.out.println("Executing after Suite ...");
	}
	
	@BeforeClass(alwaysRun=true)
	public void beforeClass()
	{
		System.out.println("Executing before class ...");
	}
	
	@AfterClass(alwaysRun=true)
	public void afterClass()
	{
		System.out.println("Executing after class ...");
	}
	
	@BeforeTest(alwaysRun=true)
	public void beforeTest()
	{
		System.out.println("Executing before test ...");
	}
	
	@AfterTest(alwaysRun=true)
	public void afterTest()
	{
		System.out.println("Executing after test ...");
	}

}
