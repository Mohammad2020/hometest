package com.testng.test.base;

import org.testng.annotations.DataProvider;

public class DataProviderClass {
	
	@DataProvider
	public static Object[][] getData()
	{
		Object [][] data=new Object[2][2];
		data[0][0]="Firefox";
		data[0][1]="Windows";
		data[1][0]="Chromw";
		data[1][1]="MAC";
				
		return data;
	}

}
