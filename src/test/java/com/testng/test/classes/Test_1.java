package com.testng.test.classes;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.testng.test.base.BaseTest;
import com.testng.test.base.DataProviderClass;

public class Test_1 extends BaseTest{
	
	@Test(groups={"group1"})
	@Parameters({"browser","OS"})
	public void test_1(String x,String y)
	{
		System.out.println("Browser: "+x);
		System.out.println("OS: "+y);
		System.out.println("Inside Test_1.Test_1");
	}
	
	@Test(groups={"group2"},dataProvider="getData", dataProviderClass=DataProviderClass.class)
	public void test_2(String a,String b)
	{
		System.out.println(a);
		System.out.println(b);
		System.out.println("Inside Test_1.Test_2");
	}
}
