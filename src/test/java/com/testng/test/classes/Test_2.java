package com.testng.test.classes;


import org.testng.Reporter;
import org.testng.annotations.Test;

import com.testng.test.base.BaseTest;
import com.testng.test.base.DataProviderClass;

public class Test_2 extends BaseTest{

	
	@Test(groups={"group1","group3"})
	public void test_1()
	{
		String value = Reporter.getCurrentTestResult().getTestContext().getCurrentXmlTest().getParameter("Car");
		System.err.println("webdriver.deviceName.iPhone = " + value);
		System.out.println("Inside Test_2.Test_1");
	}
	
	@Test(groups={"group1"},dataProvider="getData", dataProviderClass=DataProviderClass.class)
	public void test_2(String a,String b)
	{
		System.out.println(a);
		System.out.println(b);
		System.out.println("Inside Test_2.Test_2");
	}
}
